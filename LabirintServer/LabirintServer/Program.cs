﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace LabirintServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Listener serv = new Listener(IPAddress.Parse("127.0.0.1"), 1337);

            serv.StartListen();
        }
    }
}
