﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabirintServer
{
    class Player
    {
        public string name { get; set; } // Сгенерированное имя

        public string IpAddress { get; set; } // Ip адрес пользователя
        public string port { get; set; }// Порт, на который пользователь ждёт сообщения

        public Player(string nm, string IP, string prt)
        {
            this.name = nm;
            this.IpAddress = IP;
            this.port = prt;
        }
    }
}
