﻿using System;
using System.Net.Sockets;
using System.Net;
using SeriazbleDll;
using System.Collections.Generic;

namespace LabirintServer
{
    // Слушает входящие сообщения и выполняет требуемые действия
    class Listener
    {
        private List<Player> players;

        private IPAddress LocalIp; // IP сервера
        private int port; // Прослушиваемый порт
        private TcpListener Listen;

        public Listener(IPAddress ip, int p)
        {
            players = new List<Player>();
            LocalIp = ip;
            port = p;

            Listen = new TcpListener(ip, p); // Инициализируем слушателя
        }

        // Начинаем слушать
        public void StartListen()
        {
            Listen.Start(); // Начинаем слушать
            Byte[] bytes;
            DataSend data;

            Serizable ser = new Serizable();

            while (true)
            {
                Console.WriteLine("Ожидаю подключения...");
                TcpClient client = Listen.AcceptTcpClient(); // Если перехватили подключение
                Console.WriteLine("Получил подключение...");

                NetworkStream stream = client.GetStream(); // Получаем поток
                bytes = new Byte[512];
                int count = stream.Read(bytes, 0, bytes.Length);

                data = new DataSend();
                data = ser.DeSerilizabel(bytes); // Приняли команду

                // Дальше будут действия
                data = Answer(data); // Получили ответ

                SendMessage(data.GetArg()[0], Convert.ToInt32(data.GetArg()[1]), data);
            }
        }

        private DataSend Answer(DataSend data)
        {
            switch (data.GetComand())
            {
                case "GiveName": // Получить имя для игры
                    {
                        // Для получения имени: [ip]->[port];
                        List<string> temp = data.GetArg(); // Получаем аргументы

                        string name = Convert.ToString(players.Count); // Получили IP

                        players.Add(new Player(name, temp[0], temp[1])); // Добавили в списки

                        // Формируем ответ
                        string ans = "Your name = " + name;

                        DataSend answer = new DataSend(ans, temp);
                        return answer;
                    }
            }
            return null;
        }
        public void SendMessage(String serv, int porg, DataSend Message)
        {
            TcpClient Tcp = new TcpClient(serv, port);

            Serizable ser = new Serizable();
            Byte[] data = ser.Serilizable(Message);
            Console.WriteLine("Байтов: " + data.Length);

            NetworkStream stream = Tcp.GetStream();
            stream.Write(data, 0, data.Length);

            Console.WriteLine("Отправлено: ");
            Message.Print();
        }
    }
}
