﻿using System;
using System.Net.Sockets;
using SeriazbleDll;
using System.Net.Sockets;

namespace ConsoleClient
{
    // Занимается отправкой сообщений
    class Client
    {
        private TcpClient Tcp;
        private String srv;
        private int port;

        private int prt; // Порт, на который будут приниматься ответы


        public Client(String serv, int port, int prt)
        {
            this.srv = serv;
            this.port = port;
            this.prt = prt;
        }

        // Запускает кликет
        private void Init()
        {
            Tcp = new TcpClient(srv, port);
        }


        /// <summary>
        /// Отправка сообщения  
        /// </summary>
        /// <param name="Message">Отправляемое сообщение</param>
        /// <returns>Ответ типа DataSend</returns>
        public DataSend SendMessage(DataSend Message)
        {
            Init();
            Serizable ser = new Serizable();
            Byte[] data = ser.Serilizable(Message);
            Console.WriteLine("Байтов: " + data.Length);

            NetworkStream stream = Tcp.GetStream();
            stream.Write(data, 0, data.Length);

            Console.WriteLine("Отправлено: ");
            Message.Print();
            stream.Close();
            Tcp.Close();

           return WaitAnswer(); // Ожидаем ответа
        }

        /// <summary>
        /// Ожидание ответа
        /// </summary>
        /// <returns>Ответ типа DataSend</returns>
        public DataSend WaitAnswer()
        {
            TcpListener temp = new TcpListener(prt);
            temp.Start();

            Byte[] bytes;
            DataSend data;
            Serizable ser = new Serizable();

            Console.WriteLine("Ожидаю ответ...");
            TcpClient client = temp.AcceptTcpClient(); // Если перехватили подключение
            Console.WriteLine("Получил ответ...");

            NetworkStream stream = client.GetStream(); // Получаем поток
            bytes = new Byte[512];
            int count = stream.Read(bytes, 0, bytes.Length);

            data = new DataSend();
            data = ser.DeSerilizabel(bytes); // Приняли ответ

            Console.WriteLine("Ответ: ");
            data.Print();
            return data;
        }
    }
}
