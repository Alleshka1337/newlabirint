﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;

namespace SeriazbleDll
{
    // структура класса команд, для передачи
    [Serializable]
    public class DataSend
    {
        private string Comand; // Команда
        private List<string> Arg; // Параметры

        public DataSend()
        {
            this.Comand = null;
            this.Arg = null;
        }
        public DataSend(string com)
        {
            this.Comand = com;
            Arg = new List<string>();
        }
        public DataSend(string com, List<string> ar)
        {
            this.Comand = com;
            this.Arg = ar;
        }

        public string GetComand()
        {
            return this.Comand;
        }
        public List<string> GetArg()
        {
            return this.Arg;
        }

        public void Print()
        {
            Console.WriteLine("Comand: " + GetComand());
            Console.WriteLine("Arg: ");
            for (int i = 0; i < GetArg().Count; i++)
            {
                Console.WriteLine(GetArg()[i]);
            }
        }
    }
    
    public class Serizable
    {
        /// <summary>
        /// Сериализация команды
        /// </summary>
        /// <param name="data">Пакет</param>
        /// <returns>Поток байтов</returns>
        public byte[] Serilizable(DataSend data)
        {
            BinaryFormatter bin = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            bin.Serialize(stream, data);

            byte[] buffer = stream.GetBuffer();
            return buffer;
        }

        /// <summary>
        /// Десериализация
        /// </summary>
        /// <param name="data">Поток байт</param>
        /// <returns>Объект DataSend</returns>
        public DataSend DeSerilizabel(byte[] data)
        {
            // Console.WriteLine("Получено байт: " + data.Length);
            BinaryFormatter bin = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();

            stream.Write(data, 0, data.Length); // Записываем данные в поток
            stream.Seek(0, SeekOrigin.Begin);
            DataSend temp;

            temp = (DataSend)bin.Deserialize(stream); // Десериализуем
            return temp;
        }
    }
}
